
const express = require("express");

const app = express();

const port = 3000;

app.use(express.json())
app.use(express.urlencoded({extended: true}));


let users = 
    {
        "username" : 'johndoe', 
        "password" : 'johndoe123'
    }

// a GET route that will access the "/home" route
app.get('/home', (request,response) => {
    response.send('Welcome to the home page')
})

//  a GET route that will access the "/users"
app.get ('/users', (request,response) => {
    response.send(users)
})


//a delete route that will access the "/delete-user" route

app.delete('/delete-user', (request,response) => {
let message;
    {
    message = "The user has been deleted"
}
response.send(message);
})







app.listen(port, () => console.log(`Server running at port ${port}`))
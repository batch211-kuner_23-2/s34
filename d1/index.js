/*
-	use the "require" directive to load the express module/package
-	a "module" is a software component or part of a program that contains one or more routines
-	this is used to get the contents of the package to be used by our application.
-	it also allows us to access methods and functions that will allow us to easily create a server
*/
const express = require("express");

/*
-	create an application using express
-	this creates an express application and stores this in a constant called app
-	in layman's term, app is our server
*/
const app = express();

// for our application server to run, we need a port to listen top
const port = 3000;

// MIDDLEWARES

/*
-	setup for allowing the server to handle data from request
-	allows your app to read json data
-	methods used from express.js are middlewares
-	Middleware is a layer of software that enables interaction and transmission of information between assorted applications.
*/
app.use(express.json())
/*
-	allows your app to read data from forms
-	by default, information received from the URL can only be received as a string or an array
-	by applying the option of "extended: true", we are allowed to receive information in other data types such as an object throughout our application
*/
app.use(express.urlencoded({extended: true}));

// ROUTES
/*
-	express has methods corresponding to each HTTP method
-	this route expects to receive a GET request at the base URI
*/

// RETURNS SIMPLE MESSAGE
app.get('/', (request,response) => {
	response.send('Hello World')
})

app.listen(port, () => console.log(`Server running at port ${port}`))


// RETURN SIMPLE MESSAGE
/*
URI: /hello
method: GET

POSTMAN:
url: http://localhost:3000/hello
method: GET
*/

app.get ('/hello', (request,response) => {
	response.send('Hello from the "/hello" endpoint')
})

// RETURN SIMPLE GREETING
/*
URI: /hello
method: POST

POSTMAN:
url: http://localhost:3000/hello
method: POST
body: raw + json
{
	"firstName": "Nehemiah",
	"lastName": "Ellorico"
}
*/

app.post('/hello', (request,response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}! This is from the "/hello" endpoint but with a post method`)
})

let users = []

// REGISTER USER ROUTE
/*
URI: /hello
method: POST

POSTMAN:
url: http://localhost:3000/register
method: POST
body: raw + json
{
	"username": "Nehemiah",
	"password": " "
}
*/

app.post('/register', (request,response) => {

	if (request.body.username !== '' && request.body.password !== '') {
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered!`)
		console.log(request.body)
	} else {
		response.send('Please input both username and password')
	}
})


// CHANGE PASSWORD ROUTE
/*
URI: /change-password
method: POST

POSTMAN:
url: http://localhost:3000/change-password
method: PUT
body: raw + json
{
	"username": "Nehemiah",
	"password": "Nehemiah123"
}
*/

app.put('/change-password', (request,response) => {

	let message;

	for (let i = 0; i < users.length; i++) {

		if (request.body.username == users[i].username) {
			users[i].password = request.body.password
			message = `User ${request.body.username}'s password has been updated!`
			
			break;

		} else {
			message = "User does not exist"
		}
	}
	response.send(message);
})